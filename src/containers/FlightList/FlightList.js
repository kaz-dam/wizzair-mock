import React, { Component } from 'react';
import { connect } from "react-redux";

import "./FlightList.css";
import Aux from "../../hoc/Aux";
import FlightListItem from "../../components/FlightListItem/FlightListItem";
import Button from "../../components/UI/Button/Button";

class FlightList extends Component {
    onPickDateClicked = () => {
        // FELUGRÓ POPUP BOX A DÁTUMVÁLASZTÓ INPUT ELEMMEL
        console.log('Picked a return date');
    }

    render() {
        let departureResults = null;
        let returnResults = null;

        if (this.props.tickets.length) {
            departureResults = (
                <Aux>
                    <h2>Results for departure date</h2>
                    <div className="FlightSearchApp__SearchResults__There">
                        {this.props.tickets.map(ticket => {
                            // AZ EGYES JEGYEKRŐL AZ INFORMÁCIÓKAT ITT LEHETNE PROPERTY-KÉNT A KOMPONENSNEK PASSZOLNI
                            return <FlightListItem key={ticket.id} />;
                        })}
                    </div>
                </Aux>
            );
        }
        
        if (this.props.noTickets) {
            departureResults = (
                <Aux>
                    <h2>No tickets found on this date!</h2>
                </Aux>
            );
        }

        if (!this.props.returnDate && this.props.tickets.length) {
            returnResults = (
                <Aux>
                    <h2>You didn't choose return date</h2>
                    <div className="FlightSearchApp__SearchResults__Back">
                        <Button click={this.onPickDateClicked}>I'D LIKE TO PICK A DATE</Button>
                    </div>
                </Aux>
            );
        }

        if (this.props.returnTickets.length) {
            returnResults = (
                <Aux>
                    <h2>Results for return date</h2>
                    <div className="FlightSearchApp__SearchResults__Back">
                        {this.props.returnTickets.map(ticket => {
                            // AZ EGYES JEGYEKRŐL AZ INFORMÁCIÓKAT ITT LEHETNE PROPERTY-KÉNT A KOMPONENSNEK PASSZOLNI
                            return <FlightListItem key={ticket.id} />;
                        })}
                    </div>
                </Aux>
            );
        }
        
        if (this.props.noReturnTickets) {
            returnResults = (
                <Aux>
                    <h2>No tickets found on this date!</h2>
                </Aux>
            );
        }

        return (
            <div className="FlightSearchApp__SearchResults">
                {departureResults}
                {returnResults}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loadingTickets: state.flightList.loadingTickets,
        noTickets: state.flightList.noFlightFound,
        noReturnTickets: state.flightList.noReturnFlightFound,
        tickets: state.flightList.tickets,
        returnTickets: state.flightList.returnTickets,
        returnDate: state.flightSearch.inputs.returnDate.value,
        departureDate: state.flightSearch.inputs.takeOffDate.value
    };
};

export default connect(mapStateToProps, null)(FlightList);