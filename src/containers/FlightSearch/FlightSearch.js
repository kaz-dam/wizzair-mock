import React, { Component } from 'react';
import DatePicker from "react-date-picker";
import { connect } from "react-redux";

import "./FlightSearch.css";
import Button from '../../components/UI/Button/Button';
import Input from '../../components/UI/Input/Input';
import Spinner from "../../components/UI/Spinner/Spinner";
import * as utilities from "../../shared/utility";
import * as allActions from "../../store/actions/index";
import FlightList from "../FlightList/FlightList";

class FlightSearch extends Component {
    state = {
        fromFiltered: [],
        toFiltered: [],
        fromFieldEmpty: true,
        inputs: {
            from: {
                isTouched: false,
                isValid: true,
                label: 'From',
                inputName: 'from_input'
            },
            to: {
                isTouched: false,
                isValid: true,
                label: 'To',
                inputName: 'to_input'
            },
            takeOffDate: {
                isTouched: false,
                isValid: true
            },
            returnDate: {
                isTouched: false,
                isValid: true
            }
        }
    }

    componentDidMount() {
        if (!this.props.origins.length) {
            this.props.fetchStations(this.props.location);
        }
    }

    // KERESŐ FORM SUBMIT
    onSubmit = (e) => {
        e.preventDefault();
        const formData = {};
        for (let input in this.props.inputValues) {
            if (input === 'from' || input === 'to') {
                formData[input] = utilities.getQueryParam(this.props.location, input);
            } else {
                formData[input] = this.props.inputValues[input].value;
            }
        }
        
        if (utilities.validateFields(formData)) {
            this.props.getTickets(formData);
        }
    }

    // INPUT MEZŐK ONCHANGE EVENT FIGYELŐ
    onInputChange = (e, input) => {
        const searchText = e.target.value.toLowerCase();
        const newArray = input === 'from_input' ? [...this.props.origins] : [...this.props.possibleDestionations];
        const filtered = newArray.filter(item => {
            const loweredName = item.shortName.toLowerCase();
            return loweredName.startsWith(searchText);
        });

        this.props.setInputValue(searchText, input);

        // LOKÁLIS UI STATE MANAGEMENT
        if (input === 'from_input') {
            this.setState({
                fromFiltered: filtered,
                fromFieldEmpty: false,
                inputs: utilities.updateObject(this.state.inputs, {from: utilities.updateObject(this.state.inputs.from, {isTouched: true})})
            });
        } else {
            this.setState({
                toFiltered: filtered,
                fromFieldEmpty: this.state.inputs.from.value === '',
                inputs: utilities.updateObject(this.state.inputs, {to: utilities.updateObject(this.state.inputs.to, {isTouched: true})})
            });
        }
    }

    // ÁLLOMÁS VÁLASZTÁSA A LEGÖRDÜLŐ MENÜBŐL
    onDropdownItemSelected = (item, input) => {
        this.props.history.push({
            ...this.props.location,
            search: '?' + utilities.setQueryParams(this.props, {input: input, iata: item.iata})
        });

        if (input === 'from_input') {
            this.props.getDestinations(item);
            this.props.setInputValue(item.shortName, input);

            // LOKÁLIS UI STATE MANAGEMENT
            this.setState({
                toFiltered: [],
                fromFieldEmpty: false,
                inputs: utilities.updateObject(this.state.inputs, {from: utilities.updateObject(this.state.inputs.from, {isTouched: true})})
            });
        } else {
            this.props.setInputValue(item.shortName, input);

            // LOKÁLIS UI STATE MANAGEMENT
            this.setState({
                fromFieldEmpty: this.props.inputValues.from.value === '',
                inputs: utilities.updateObject(this.state.inputs, {to: utilities.updateObject(this.state.inputs.to, {isTouched: true})})
            });
        }
    }

    // DÁTUM KIVÁLASZTÁSA
    onDatePicked = (date, which) => {
        if (which === 'takeOff') {
            this.props.setInputValue(date, which);
            // LOKÁLIS UI STATE MANAGEMENT
            this.setState({
                inputs: utilities.updateObject(this.state.inputs, { takeOffDate: utilities.updateObject(this.state.inputs.takeOffDate, { isTouched: true }) })
            });
        } else {
            this.props.setInputValue(date, which);
            // LOKÁLIS UI STATE MANAGEMENT
            this.setState({
                inputs: utilities.updateObject(this.state.inputs, { returnDate: utilities.updateObject(this.state.inputs.returnDate, { isTouched: true }) })
            });
        }
    }

    render() {
        return (
            <div className="FlightSearchApp">
                <div className="FlightSearchApp__SearchContainer">
                    <form onSubmit={this.onSubmit}>
                        <h1>Flight Search</h1>
                        <div className="FlightSearchApp__SearchContainer__InputBox">
                            <Input
                                type="text"
                                onTyping={this.onInputChange}
                                isValid={this.state.inputs.from.isTouched ? utilities.validateFields({from: this.props.inputValues.from.value}) : true}
                                dropdownArray={this.state.fromFiltered.length ? this.state.fromFiltered : this.props.origins}
                                dropdownSelected={this.onDropdownItemSelected}
                                label={this.state.inputs.from.label}
                                inputName={this.state.inputs.from.inputName}
                                inputValue={this.props.inputValues.from.value} />
                            <Input
                                type="text"
                                onTyping={this.onInputChange}
                                fromIsEmpty={this.state.fromFieldEmpty && this.state.inputs.to.isTouched}
                                isValid={this.state.inputs.to.isTouched ? utilities.validateFields({to: this.props.inputValues.to.value}) : true}
                                dropdownArray={this.state.toFiltered.length ? this.state.toFiltered : this.props.possibleDestionations}
                                dropdownSelected={this.onDropdownItemSelected}
                                label={this.state.inputs.to.label}
                                inputName={this.state.inputs.to.inputName}
                                inputValue={this.props.inputValues.to.value} />
                        </div>
                        <div className="FlightSearchApp__SearchContainer__InputBox">
                            <div className="DateBox">
                                <DatePicker
                                    onChange={(date) => this.onDatePicked(date, 'takeOff')}
                                    value={this.props.inputValues.takeOffDate.value}
                                    minDate={new Date()}
                                    locale="en-EN" />
                                {this.state.inputs.takeOffDate.isTouched && !this.props.inputValues.takeOffDate.value
                                ? <p>Choose departure date!</p>
                                : null}
                            </div>
                            <div className="DateBox">
                                <DatePicker
                                    onChange={(date) => this.onDatePicked(date, 'return')}
                                    value={this.props.inputValues.returnDate.value}
                                    minDate={this.props.inputValues.takeOffDate.value ? new Date(this.props.inputValues.takeOffDate.value.getTime() + 86400000) : null}
                                    locale="en-EN" />
                                {this.state.inputs.returnDate.isTouched && !this.props.inputValues.returnDate.value
                                ? <p>Choose departure date!</p>
                                : null}
                            </div>
                        </div>
                        <Button type="submit">SEARCH</Button>
                    </form>
                </div>
                {this.props.loadingTickets ? <Spinner /> : <FlightList />}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        inputValues: state.flightSearch.inputs,
        origins: state.flightSearch.from,
        possibleDestionations: state.flightSearch.to,
        loading: state.flightSearch.loadingStations,
        loadingTickets: state.flightList.loadingTickets
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchStations: (location) => dispatch(allActions.fetchStations(location)),
        setInputValue: (inputValue, inputName) => dispatch(allActions.setInputValue(inputValue, inputName)),
        getDestinations: (item) => dispatch(allActions.setAvailableConnections(item)),
        getTickets: (searchParam) => dispatch(allActions.fetchTickets(searchParam))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightSearch);