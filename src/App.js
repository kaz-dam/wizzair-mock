import React, { Component } from 'react';
import { BrowserRouter, Route } from "react-router-dom";

import FlightSearch from './containers/FlightSearch/FlightSearch';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Route path="/" component={FlightSearch} />
      </BrowserRouter>
    );
  }
}

export default App;
