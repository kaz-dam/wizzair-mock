import React from 'react';

import "./Button.css";

const Button = (props) => {
    return (
        <button type={props.type} onClick={props.click} className="UI__Button">
            {props.children}
        </button>
    );
}

export default Button;