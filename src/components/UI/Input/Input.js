import React, { Component } from 'react';

import "./Input.css";
import Spinner from "../Spinner/Spinner";

class Input extends Component {
    state = {
        focused: false,
        errorMessages: {
            chooseOriginFirst: 'Please choose origin before the destination!',
            chooseFromDropdown: 'Please choose origin/destination from the list!'
        }
    }

    onFocused = () => {
        this.setState({ focused: true });
    }

    onBlured = () => {
        setTimeout(() => {
            this.setState({ focused: false });
        }, 200);
    }

    render() {
        let dropdown = null;
        let validationMessage = null;

        if (this.state.focused) {
            if (!this.props.dropdownArray.length) {
                dropdown = (<div className="UI__Input__Select"><Spinner /></div>);
            } else {
                dropdown = (<div className="UI__Input__Select">
                    {this.props.dropdownArray.map(item => {
                        return <span
                                    key={item.iata}
                                    onClick={() => this.props.dropdownSelected(item, this.props.inputName)}>
                                        {item.shortName} ({item.iata})</span>;
                    })}
                </div>);
            }
        }

        if (!this.props.isValid) {
            validationMessage = <span className="UI__Input__ErrorMsg">{this.state.errorMessages.chooseFromDropdown}</span>;
        }

        if (this.props.fromIsEmpty) {
            validationMessage = <span className="UI__Input__ErrorMsg">{this.state.errorMessages.chooseOriginFirst}</span>;
        }

        return (
            <div className="UI__Input__Container">
                <label htmlFor={this.props.inputName}>{this.props.label}</label>
                <input
                    className="UI__Input"
                    id={this.props.inputName}
                    name={this.props.inputName}
                    onFocus={this.onFocused}
                    onBlur={this.onBlured}
                    onChange={(e) => this.props.onTyping(e, this.props.inputName)}
                    type={this.props.type}
                    value={this.props.inputValue}
                    autoComplete="off" />
                {dropdown}
                {validationMessage}
            </div>
        );
    }
}

export default Input;