import React from "react";

import "./Spinner.css";

const Spinner = (props) => {
    return <div className="UI__Spinner"></div>;
};

export default Spinner;