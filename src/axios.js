import axios from "axios";

const instance = axios.create({
    baseURL: 'https://mock-air.herokuapp.com'
});

export default instance;