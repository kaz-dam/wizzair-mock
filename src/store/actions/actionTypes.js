export const INIT_STATIONS = 'INIT_STATIONS';
export const SET_STATIONS = 'SET_STATIONS';
export const SET_STATIONS_WITH_INPUT_VALUES = 'SET_STATIONS_WITH_INPUT_VALUES';
export const SET_INPUT_VALUE = 'SET_INPUT_VALUE';
export const SET_AVAILABLE_CONNECTIONS = 'SET_AVAILABLE_CONNECTIONS';

export const INIT_TICKETS = 'INIT_TICKETS';
export const SET_TICKETS = 'SET_TICKETS';
export const SET_RETURN_TICKETS = 'SET_RETURN_TICKETS';
export const NO_TICKETS_FOUND = 'NO_TICKETS_FOUND';
export const NO_RETURN_TICKETS_FOUND = 'NO_RETURN_TICKETS_FOUND';