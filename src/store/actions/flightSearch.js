import * as actionTypes from "./actionTypes";
import axios from "../../axios";

export const initStations = () => {
    return {
        type: actionTypes.INIT_STATIONS
    };
};

export const setStations = (stations) => {
    return {
        type: actionTypes.SET_STATIONS,
        allStations: stations
    }
};

export const setStationsWithInputValues = (stations, location) => {
    return {
        type: actionTypes.SET_STATIONS_WITH_INPUT_VALUES,
        allStations: stations,
        location: location
    };
};

export const setInputValue = (value, whichInput) => {
    return {
        type: actionTypes.SET_INPUT_VALUE,
        value: value,
        inputName: whichInput
    };
};

export const setAvailableConnections = (item) => {
    return {
        type: actionTypes.SET_AVAILABLE_CONNECTIONS,
        chosenOrigin: item
    };
};

export const fetchStations = (location) => {
    return dispatch => {
        dispatch(initStations());
        axios.get('/asset/stations')
            .then(res => {
                if (location.search) {
                    dispatch(setStationsWithInputValues(res.data, location));
                } else {
                    dispatch(setStations(res.data));
                }
            });
    };
};