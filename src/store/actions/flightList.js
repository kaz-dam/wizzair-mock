import * as actionTypes from "./actionTypes";
import axios from "../../axios";
import * as utilities from "../../shared/utility";

export const initTickets = () => {
    return {
        type: actionTypes.INIT_TICKETS
    };
};

export const setTickets = (tickets) => {
    return {
        type: actionTypes.SET_TICKETS,
        tickets: tickets
    }
};

export const setReturnTickets = (returnTickets) => {
    return {
        type: actionTypes.SET_RETURN_TICKETS,
        returnTickets: returnTickets
    };
};

export const noTicketsFound = () => {
    return {
        type: actionTypes.NO_TICKETS_FOUND
    };
};

export const noReturnTicketsFound = () => {
    return {
        type: actionTypes.NO_RETURN_TICKETS_FOUND
    };
};

export const fetchTickets = (searchParams) => {
    return dispatch => {
        dispatch(initTickets());
        axios.get('/search', {
            params: {
                departureStation: searchParams.from,
                arrivalStation: searchParams.to,
                date: utilities.formatDate(searchParams.takeOffDate)
            }
        })
            .then(res => {
                if (res.data.length) {
                    dispatch(setTickets(res.data));
                } else {
                    dispatch(noTicketsFound());
                }
            })
            .catch(err => console.log(err));

        if (searchParams.returnDate) {
            dispatch(initTickets());
            axios.get('/search', {
                params: {
                    departureStation: searchParams.to,
                    arrivalStation: searchParams.from,
                    date: utilities.formatDate(searchParams.returnDate)
                }
            })
                .then(res => {
                    if (res.data.length) {
                        dispatch(setReturnTickets(res.data));
                    } else {
                        dispatch(noReturnTicketsFound());
                    }
                })
                .catch(err => console.log(err));
        }
    };
};