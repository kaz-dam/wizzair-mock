import * as actionTypes from "../actions/actionTypes";
import * as utilities from "../../shared/utility";

const initialState = {
    tickets: [],
    returnTickets: [],
    loadingTickets: false,
    noFlightFound: false,
    noReturnFlightFound: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_TICKETS:
            return utilities.updateObject(state, {loadingTickets: true});
        case actionTypes.SET_TICKETS:
            return utilities.updateObject(state, {loadingTickets: false, tickets: action.tickets});
        case actionTypes.SET_RETURN_TICKETS:
            return utilities.updateObject(state, {loadingTickets: false, returnTickets: action.returnTickets});
        case actionTypes.NO_TICKETS_FOUND:
            return utilities.updateObject(state, {noFlightFound: true, loadingTickets: false});
        case actionTypes.NO_RETURN_TICKETS_FOUND:
            return utilities.updateObject(state, {noReturnFlightFound: true, loadingTickets: false});
        default:
            return state;
    }
};

export default reducer;