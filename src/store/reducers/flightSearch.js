import * as actionTypes from '../actions/actionTypes';
import * as utilities from "../../shared/utility";

const initialState = {
    from: [],
    to: [],
    loadingStations: false,
    inputs: {
        from: {
            value: ''
        },
        to: {
            value: ''
        },
        takeOffDate: {
            value: null
        },
        returnDate: {
            value: null
        }
    }
};

const setStationsWithInputValues = (state, action) => {
    const valuesFromQueryParam = utilities.setInputsFromQueryParam(action.location, action.allStations);
    const updatedFromObject = utilities.updateObject(state.inputs.from, {value: valuesFromQueryParam['from_input']});
    const updatedToObject = utilities.updateObject(state.inputs.to, {value: valuesFromQueryParam['to_input']});
    const updatedInputsObject = utilities.updateObject(state.inputs, {from: updatedFromObject, to: updatedToObject});
    return utilities.updateObject(state, {inputs: updatedInputsObject, from: action.allStations});
};

const setAvailableConnections = (state, action) => {
    const destinations = utilities.getConnections(action.chosenOrigin, state.from);
    return utilities.updateObject(state, {to: destinations});
};

const setInputValue = (state, action) => {
    let updatedInputObject = {};

    switch (action.inputName) {
        case 'from_input':
            updatedInputObject = {from: utilities.updateObject(state.inputs.from, {value: action.value})};
            break;
        case 'to_input':
            updatedInputObject = {to: utilities.updateObject(state.inputs.to, {value: action.value})};
            break;
        case 'takeOff':
            updatedInputObject = {takeOffDate: utilities.updateObject(state.inputs.takeOffDate, {value: action.value})};
            break;
        case 'return':
            updatedInputObject = {returnDate: utilities.updateObject(state.inputs.returnDate, {value: action.value})};
            break;
        default:
            break;
    }

    const updatedInputs = {inputs: utilities.updateObject(state.inputs, updatedInputObject)};
    return utilities.updateObject(state, updatedInputs);
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_STATIONS:
            return utilities.updateObject(state, {loadingStations: true});
        case actionTypes.SET_STATIONS:
            return utilities.updateObject(state, {from: action.allStations, loadingStations: false});
        case actionTypes.SET_STATIONS_WITH_INPUT_VALUES:
            return setStationsWithInputValues(state, action);
        case actionTypes.SET_INPUT_VALUE:
            return setInputValue(state, action);
        case actionTypes.SET_AVAILABLE_CONNECTIONS:
            return setAvailableConnections(state, action);
        default:
            return state;
    }
}

export default reducer;