export const updateObject = (oldObj, newValues) => {
    return {
        ...oldObj,
        ...newValues
    };
};

// ADOTT VÁROSHOZ TARTOZÓ CÉLVÁROS KIKERESÉSE
export const getConnections = (fromObj, allStations) => {
    return fromObj.connections.reduce((newArr, item) => {
        for (let i = 0; i < allStations.length; i++) {
            if (allStations[i].iata === item.iata) {
                newArr.push(allStations[i]);
            }
        }
        return newArr;
    }, []);
};

// INPUT MEZŐ VALIDÁLÁS
export const validateFields = (inputsObj, allStations) => {
    let isAllInputValid = true;
    for (let input in inputsObj) {
        if (input === 'takeOffDate') {
            isAllInputValid = inputsObj[input] !== null && isAllInputValid;
        }

        if (input === 'from' || input === 'to') {
            isAllInputValid = inputsObj[input] !== '' && isAllInputValid;
        }

        if (allStations) {
            for (const station of allStations) {
                if (input === 'from' || input === 'to') {
                    isAllInputValid = inputsObj[input] === station.shortName.toLowerCase() && isAllInputValid;
                }
            }
        }
    }
    return isAllInputValid;
};

// KERESÉSI PARAMÉTEREK BEÁLLÍTÁSA
export const setQueryParams = (props, inputData) => {
    const searchQuery = new URLSearchParams(props.location.search);
    if (searchQuery.has(inputData.input)) {
        searchQuery.delete(inputData.input);
        searchQuery.append(inputData.input, inputData.iata);
    } else {
        searchQuery.append(inputData.input, inputData.iata);
    }
    return searchQuery.toString();
};

// OLDAL UJRATÖLTÉSNÉL KERESÉSI PARAMÉTEREK ALAPJÁN INPUT MEZŐ KITÖLTÉSE
export const setInputsFromQueryParam = (location, allStations) => {
    const values = {};
    const searchQ = new URLSearchParams(location.search).entries();
    for (let p of searchQ) {
        allStations.forEach(station => {
            if (station.iata === p[1]) {
                values[p[0]] = station.shortName;
            }
        });
    }
    return values;
};

// EGY ADOTT KERESÉSI PARAMÉTER KINYERÉSE
export const getQueryParam = (location, input) => {
    const searchQ = new URLSearchParams(location.search).get(input + '_input');
    return searchQ;
};

// DÁTUM FORMÁZÁS A KÉRÉS ELKÜLDÉSÉHEZ A BACKEND-NEK
export const formatDate = (dateObj) => {
    const year = dateObj.getFullYear();
    const month = (dateObj.getMonth() + 1) < 10 ? '0' + (dateObj.getMonth() + 1) : (dateObj.getMonth() + 1);
    const date = dateObj.getDate();

    return year + '-' + month + '-' + date;
};